import {RequestHandler} from "express";
import {squareDigitsSequence} from "./SquareSequance";

export const SquareNumber: RequestHandler = (req, res) => {
     const output = squareDigitsSequence(req.body.initNumber);
     res.json(output);
}