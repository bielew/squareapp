export function squareDigitsSequence(num: number) {
    if (+num > 650 || +num < 1){
        console.log("Error!: input out of range (it must be between 1 and 650)")
    }
    else {
        return squareCounter(+num);
    }
    function squareCounter(num: number): number {
        const SequenceOfSquares: number[] = [];
        let lastValue = num;
        while (!SequenceOfSquares.includes(lastValue)){
            SequenceOfSquares.push(lastValue);
            lastValue = squareSum(lastValue);
        }
        return SequenceOfSquares.length + 1;
    }

    function squareSum(numbers: number): number {
        const result = numberToArray(numbers)
        return result.map(x => x*x).reduce((a, b) => a + b , 0);
    }

    function numberToArray(numbers: number): number[]{
        return numbers.toString().split('').map((val: string) => parseInt(val));
    }
}