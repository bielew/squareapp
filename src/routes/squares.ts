import { Router } from 'express';
import { SquareNumber } from "../controllers/squares";

const router = Router();

router.post('/squares', SquareNumber);

export default router;
