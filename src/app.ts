import express from 'express';
import { json } from 'body-parser'
import squareRoutes from './routes/squares';

const cors = require('cors');

const app = express();

app.use(cors());

app.use(json());

app.use('/squares', squareRoutes);

app.use('/', squareRoutes);




app.listen(3000);
